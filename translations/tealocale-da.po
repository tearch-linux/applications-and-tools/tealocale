# Danish translation for linuxmint
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-15 14:24+0300\n"
"PO-Revision-Date: 2019-11-14 12:59+0000\n"
"Last-Translator: scootergrisen <scootergrisen@gmail.com>\n"
"Language-Team: Danish <da@li.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2021-06-25 10:38+0000\n"
"X-Generator: Launchpad (build 40cc6dba4163ba9ca785b26ad91f43c87d70ba4d)\n"

#: src/resources/add.ui:52
msgid "Cancel"
msgstr "Annullér"

#: src/resources/add.ui:65
msgid "Install"
msgstr "Installér"

#: src/resources/add.ui:71 src/resources/add.ui:72
msgid "Install the selected language"
msgstr "Installér det valgte sprog"

#: src/resources/install_remove.ui:54
msgid "Add..."
msgstr "Tilføj …"

#: src/resources/install_remove.ui:59 src/resources/install_remove.ui:60
msgid "Add a new language"
msgstr "Tilføj et nyt sprog"

#: src/resources/install_remove.ui:70
msgid "Remove"
msgstr "Fjern"

#: src/resources/install_remove.ui:76 src/resources/install_remove.ui:77
msgid "Remove the selected language"
msgstr "Fjern det valgte sprog"

#: src/resources/install_remove.ui:100
msgid "Close"
msgstr "Luk"

#: src/tealocale.py:288
msgid "Language Settings"
msgstr "Sprogindstillinger"

#: src/tealocale.py:309
msgid "Apply System-Wide"
msgstr "Anvend for hele systemet"

#: src/tealocale.py:314
msgid "Install / Remove Languages..."
msgstr "Installér/fjern sprog …"

#: src/tealocale.py:327
msgid "Language"
msgstr "Sprog"

#: src/tealocale.py:327
msgid "Language, interface..."
msgstr "Sprog, brugerflade ..."

#: src/tealocale.py:332
msgid "Region"
msgstr "Region"

#: src/tealocale.py:332
msgid "Numbers, currency, addresses, measurement..."
msgstr "Tal, valuta, adresser, enheder …"

#: src/tealocale.py:337
msgid "Time format"
msgstr "Tidsformat"

#: src/tealocale.py:337
msgid "Date and time..."
msgstr "Dato og klokkeslæt ..."

#: src/tealocale.py:460 src/tealocale.py:461 src/tealocale.py:462
msgid "No locale defined"
msgstr "Ingen regionsinformationer defineret"

#: src/tealocale.py:549
msgid "Language:"
msgstr "Sprog:"

#: src/tealocale.py:550
msgid "Region:"
msgstr "Region:"

#: src/tealocale.py:551
msgid "Time format:"
msgstr "Tidsformat:"

#: src/tealocale.py:552
msgid "System locale"
msgstr "Systemets regionsinformationer"

#: src/tealocale.py:556
msgid "Language support"
msgstr "Sprogunderstøttelse"

#: src/tealocale.py:556
#, python-format
msgid "%d language installed"
msgid_plural "%d languages installed"
msgstr[0] "%d sprog installeret"
msgstr[1] "%d sprog installerede"

#: src/add.py:39
msgid "Add a New Language"
msgstr "Tilføj et nyt sprog"

#: src/install_remove.py:39
msgid "Install / Remove Languages"
msgstr "Tilføj/fjern sprog"

#: src/install_remove.py:163
msgid "Fully installed"
msgstr "Fuldt installeret"

#~ msgid "Already installed"
#~ msgstr "Allerede installeret"

#~ msgid "Input Method"
#~ msgstr "Inputmetode"

#~ msgid "XIM"
#~ msgstr "XIM"

#~ msgid "None"
#~ msgstr "Ingen"

#~ msgid "Some language packs are missing"
#~ msgstr "Nogle sprogpakker mangler"

#~ msgid "Input method framework:"
#~ msgstr "Inputmetoderamme:"

#~ msgid "Welcome"
#~ msgstr "Velkomst"

#~ msgid ""
#~ "Input methods are used to write symbols and characters which are not "
#~ "present on the keyboard. They are useful to write in Chinese, Japanese, "
#~ "Korean, Thai, Vietnamese..."
#~ msgstr ""
#~ "Indtastningsmetoder anvendes til at skrive symboler og tegn, som ikke er "
#~ "tilgængelige på tastaturet. De er nyttige til at skrive på kinesisk, "
#~ "japansk, koreansk, thai, vietnamesisk …"

#~ msgid ""
#~ "To add support for a particular language, select it in the sidebar and "
#~ "follow the instructions."
#~ msgstr ""
#~ "Tilføj understøttelse af et bestemt sprog ved at vælge det i sidepanelet "
#~ "og følge instruktionerne."

#~ msgid "Help"
#~ msgstr "Hjælp"

#~ msgid "To write in simplified Chinese follow the steps below:"
#~ msgstr "Følg trinnene nedenfor for at skrive på kinesisk (forenklet):"

#~ msgid "- Install the language support packages: "
#~ msgstr "- Installér pakker til understøttelse af sprog: "

#~ msgid "- Log out and log back in."
#~ msgstr "- Log af og log på igen."

#~ msgid ""
#~ "- Right-click the Fcitx applet in the system tray and choose \"Configure"
#~ "\"."
#~ msgstr ""
#~ "- Højreklik på Fcitx-panelprogrammet i statusfeltet og vælg \"Konfigurér"
#~ "\"."

#~ msgid ""
#~ "- In the Fcitx configuration screen, add the <b>Pinyin</b> input method "
#~ "(uncheck \"Only Show Current Language\" if necessary)."
#~ msgstr ""
#~ "- Tilføj <b>Pinyin</b>-inputmetoden i Fcitx-konfigurationsdialogen "
#~ "(fravælg \"Vis kun nuværende sprog\", hvis det er nødvendigt)."

#~ msgid "- Switch between input methods with <b>Ctrl+Space</b>."
#~ msgstr "- Skift mellem inputmetoder med <b>Ctrl+Mellemrum</b>."

#~ msgid "- Change the input method framework to <b>Fcitx</b>."
#~ msgstr "- Skift inputmetoderammen til <b>Fcitx</b>."

#~ msgid "Note: SunPinyin can also be used (either with Fcitx or IBus)."
#~ msgstr "Bemærk: SunPinyin kan også bruges (med enten Fcitx eller IBus)."

#~ msgid "Simplified Chinese"
#~ msgstr "Kinesisk (forenklet)"

#~ msgid "To write in traditional Chinese follow the steps below:"
#~ msgstr "Følg trinnene nedenfor for at skrive på kinesisk (traditionel):"

#~ msgid "Note: Chewing can also be used (either with Fcitx or IBus)."
#~ msgstr "Bemærk: Chewing kan også bruges (med enten Fcitx eller IBus)."

#~ msgid "Traditional Chinese"
#~ msgstr "Kinesisk (traditionel)"

#~ msgid "To write in Hiragana, Katakana and Kanji follow the steps below:"
#~ msgstr ""
#~ "Følg trinnene nedenfor for at skrive på hiragana, katakana og kanji:"

#~ msgid ""
#~ "- In the Fcitx configuration screen, add the <b>Mozc</b> input method "
#~ "(uncheck \"Only Show Current Language\" if necessary)."
#~ msgstr ""
#~ "- Tilføj <b>Mozc</b>-inputmetoden i Fcitx-konfigurationsdialogen (fravælg "
#~ "\"Vis kun nuværende sprog\", hvis det er nødvendigt)."

#~ msgid "Note: Mozc can also be used with the IBus input method."
#~ msgstr "Bemærk: Mozc kan også bruges med IBus-inputmetoden."

#~ msgid "Japanese"
#~ msgstr "Japansk"

#~ msgid "To write in Hangul follow the steps below:"
#~ msgstr "Følg trinnene nedenfor for at skrive på hangul:"

#~ msgid ""
#~ "- In the Fcitx configuration screen, add the <b>Hangul</b> input method "
#~ "(uncheck \"Only Show Current Language\" if necessary)."
#~ msgstr ""
#~ "- Tilføj <b>Hangul</b>-inputmetoden i Fcitx-konfigurationsdialogen "
#~ "(fravælg \"Vis kun nuværende sprog\", hvis det er nødvendigt)."

#~ msgid "Note: Hangul can also be used with the IBus input method."
#~ msgstr "Bemærk: Hangul kan også bruges med IBus-inputmetoden."

#~ msgid "Korean"
#~ msgstr "Koreansk"

#~ msgid "To write in Thai follow the steps below:"
#~ msgstr "Følg trinnene nedenfor for at skrive på thai:"

#~ msgid ""
#~ "- In the Fcitx configuration screen, add the <b>Thai</b> input method "
#~ "(uncheck \"Only Show Current Language\" if necessary)."
#~ msgstr ""
#~ "- Tilføj <b>Thai</b>-inputmetoden i Fcitx-konfigurationsdialogen (fravælg "
#~ "\"Vis kun nuværende sprog\", hvis det er nødvendigt)."

#~ msgid ""
#~ "Note: You can also use iBus or define a Thai keyboard layout without "
#~ "using any input method."
#~ msgstr ""
#~ "Bemærk: Du kan også brug iBus eller definere et thai-tastaturlayout uden "
#~ "at bruge nogen inputmetode."

#~ msgid "Thai"
#~ msgstr "Thai"

#~ msgid "To write in Vietnamese follow the steps below:"
#~ msgstr "Følg trinnene nedenfor for at skrive på vietnamesisk:"

#~ msgid ""
#~ "- In the Fcitx configuration screen, add the <b>Unikey</b> input method "
#~ "(uncheck \"Only Show Current Language\" if necessary)."
#~ msgstr ""
#~ "- Tilføj <b>Unikey</b>-inputmetoden i Fcitx-konfigurationsdialogen "
#~ "(fravælg \"Vis kun nuværende sprog\", hvis det er nødvendigt)."

#~ msgid "Note: Unikey can also be used with the IBus input method."
#~ msgstr "Bemærk: Unikey kan også bruges med IBus-inputmetoden."

#~ msgid "Vietnamese"
#~ msgstr "Vietnamesisk"

#~ msgid "To write in Telugu follow the steps below:"
#~ msgstr "Følg trinnene nedenfor for at skrive på telugu:"

#~ msgid "- Change the input method framework to <b>IBus</b>."
#~ msgstr "- Skift inputmetoderammen til <b>IBus</b>."

#~ msgid ""
#~ "- Right-click the IBus applet in the system tray and choose \"Preferences"
#~ "\"."
#~ msgstr ""
#~ "- Højreklik på IBus-panelprogrammet i statusfeltet og vælg \"Præferencer"
#~ "\"."

#~ msgid ""
#~ "- In the IBus configuration screen, add the <b>Telugu</b> input method."
#~ msgstr "- Tilføj <b>Telugu</b>-inputmetoden i IBus-konfigurationsdialogen."

#~ msgid "- Switch between input methods with <b>Super+Space</b>."
#~ msgstr "- Skift mellem inputmetoder med <b>Super+Mellemrum</b>."

#~ msgid "Note: Telugu can also be used with Fcitx."
#~ msgstr "Bemærk: Telugu kan også bruges med Fcitx."

#~ msgid "Telugu"
#~ msgstr "Telugu"

#~ msgid ""
#~ "<b><small>Note: Installing or upgrading language packs can trigger the "
#~ "installation of additional languages</small></b>"
#~ msgstr ""
#~ "<b><small>Bemærk: Installation eller opgradering af sprogpakker kan "
#~ "medføre installering af flere sprog</small></b>"

#~ msgid "Install language packs"
#~ msgstr "Installér sprogpakker"

#~ msgid ""
#~ "Install any missing language packs, translations files, dictionaries for "
#~ "the selected language"
#~ msgstr ""
#~ "Installér manglende sprogpakker, oversættelsesfiler og ordbøger for det "
#~ "valgte sprog"

#~ msgid "Languages"
#~ msgstr "Sprog"

#~ msgid "Language settings"
#~ msgstr "Sprogindstillinger"

#~ msgid "Input method"
#~ msgstr "Indtastningsmetode"
