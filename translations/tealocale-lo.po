# Lao translation for linuxmint
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the linuxmint package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: linuxmint\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-15 14:24+0300\n"
"PO-Revision-Date: 2015-07-30 14:01+0000\n"
"Last-Translator: Rockworld <sumoisrock@gmail.com>\n"
"Language-Team: Lao <lo@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Launchpad-Export-Date: 2021-06-25 10:38+0000\n"
"X-Generator: Launchpad (build 40cc6dba4163ba9ca785b26ad91f43c87d70ba4d)\n"

#: src/resources/add.ui:52
msgid "Cancel"
msgstr ""

#: src/resources/add.ui:65
msgid "Install"
msgstr "ຕິດຕັ້ງ"

#: src/resources/add.ui:71 src/resources/add.ui:72
msgid "Install the selected language"
msgstr "ຕິດຕັ້ງພາສາໄດ້ເລືອກໄວ້"

#: src/resources/install_remove.ui:54
msgid "Add..."
msgstr "ເພີ່ມ..."

#: src/resources/install_remove.ui:59 src/resources/install_remove.ui:60
msgid "Add a new language"
msgstr "ເພີ່ມພາສາໃຫມ່"

#: src/resources/install_remove.ui:70
msgid "Remove"
msgstr "ຍ້າຍອອກ"

#: src/resources/install_remove.ui:76 src/resources/install_remove.ui:77
msgid "Remove the selected language"
msgstr ""

#: src/resources/install_remove.ui:100
msgid "Close"
msgstr ""

#: src/tealocale.py:288
msgid "Language Settings"
msgstr "ຕັ້ງຄ່າພາສາ"

#: src/tealocale.py:309
msgid "Apply System-Wide"
msgstr ""

#: src/tealocale.py:314
msgid "Install / Remove Languages..."
msgstr ""

#: src/tealocale.py:327
msgid "Language"
msgstr "ພາສາ"

#: src/tealocale.py:327
msgid "Language, interface..."
msgstr ""

#: src/tealocale.py:332
msgid "Region"
msgstr "ພາກພື້ນ"

#: src/tealocale.py:332
msgid "Numbers, currency, addresses, measurement..."
msgstr ""

#: src/tealocale.py:337
msgid "Time format"
msgstr ""

#: src/tealocale.py:337
msgid "Date and time..."
msgstr ""

#: src/tealocale.py:460 src/tealocale.py:461 src/tealocale.py:462
msgid "No locale defined"
msgstr "ບໍໄດ້ຕັ້ງຄ່າບ່ອນຕັ້ງ"

#: src/tealocale.py:549
msgid "Language:"
msgstr ""

#: src/tealocale.py:550
msgid "Region:"
msgstr ""

#: src/tealocale.py:551
msgid "Time format:"
msgstr ""

#: src/tealocale.py:552
msgid "System locale"
msgstr "ບ່ອນຕັ້ງຂອງລະບົບ"

#: src/tealocale.py:556
msgid "Language support"
msgstr "ການສະໜັບສະໜູນພາສາ"

#: src/tealocale.py:556
#, python-format
msgid "%d language installed"
msgid_plural "%d languages installed"
msgstr[0] ""
msgstr[1] ""

#: src/add.py:39
msgid "Add a New Language"
msgstr "ເພີ່ມພາສາໃຫມ່"

#: src/install_remove.py:39
msgid "Install / Remove Languages"
msgstr "ຕິດຕັ້ງຫຼືລຶບພາສາອອກໄປ"

#: src/install_remove.py:163
msgid "Fully installed"
msgstr "ໄດ້ຕິດຕັ້ງຢ່າງເຕັມຕົວ"

#~ msgid "None"
#~ msgstr "ບໍ່ມີ"

#~ msgid "Some language packs are missing"
#~ msgstr "ບາງພາສາຫາຍໄປ"

#~ msgid ""
#~ "Input methods are used to write symbols and characters which are not "
#~ "present on the keyboard. They are useful to write in Chinese, Japanese, "
#~ "Korean, Thai, Vietnamese..."
#~ msgstr ""
#~ "ທາງເລືອກການໃສ່ຂໍ້ມູນ ໃຊ້ໃນການຂຽນສັນຍາລັກ ແລະຕົວໜັງສື ທີ່ບໍ່ມີໃນຄີບອດ. ໃຊ້ຂຽນພາສາຈີນ, ຍີ່ປຸ່ນ, "
#~ "ເກົາຫລີ, ໄທ, ຫວຽດນາມ..."

#~ msgid ""
#~ "<b><small>Note: Installing or upgrading language packs can trigger the "
#~ "installation of additional languages</small></b>"
#~ msgstr ""
#~ "<b><small>ໝາຍເຫດ: ຕິດຕັ້ງ ຫລື ລຶບພາສາ ອາດຈະມີບັນຫາກັບການຕິດຕັ້ງພາສາຫຼາຍໆພາສາ</small></"
#~ "b>"

#~ msgid "Languages"
#~ msgstr "ພາສາ"

#~ msgid "Language settings"
#~ msgstr "ຕັ້ງຄ່າພາສາ"

#~ msgid "Input method"
#~ msgstr "ທາງເລືອກການໃສ່ຂໍ້ມູນ"
