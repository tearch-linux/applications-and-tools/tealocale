DESTDIR=/
all: install buildmo

pot:
	xgettext -o tealocale.pot --from-code="utf-8" src/resources/*.ui `find src -type f -iname "*.py"`
	for file in `ls translations/*.po`; do \
            msgmerge $$file tealocale.pot -o $$file.new ; \
	    rm -f $$file ; \
	    mv $$file.new $$file ; \
	done \

buildmo:
	install -d $(DESTDIR)/usr/share/ || true
	@echo "Building the mo files"
	# WARNING: the second sed below will only works correctly with the languages that don't contain "-"
	for file in `ls translations/*.po`; do \
		lang=`echo $$file | sed 's@translations/@@' | sed 's/\.po//' | sed 's/tealocale-//'`; \
		install -d $(DESTDIR)/usr/share/locale/$$lang/LC_MESSAGES/; \
		msgfmt -o $(DESTDIR)/usr/share/locale/$$lang/LC_MESSAGES/tealocale.mo $$file; \
	done \

install:
	# Create necessary dirs if not exists
	install -d $(DESTDIR)/usr/lib/tealocale/ || true
	install -d $(DESTDIR)/usr/bin/ || true
	install -d $(DESTDIR)/usr/share/applications/ || true
	install -d $(DESTDIR)/usr/share/polkit-1/actions/ || true
	
	cp -rfv src/* $(DESTDIR)/usr/lib/tealocale/

	install -m755 data/tealocale $(DESTDIR)/usr/bin/
	install -m755 data/add-remove-locales $(DESTDIR)/usr/bin/

	install -m644 data/tealocale.desktop $(DESTDIR)/usr/share/applications/

	install -m644 data/com.tearch.tealocale.policy $(DESTDIR)/usr/share/polkit-1/actions/

uninstall:
	rm -rf $(DESTDIR)/usr/lib/tealocale/

	rm -f $(DESTDIR)/usr/bin/tealocale
	rm -f $(DESTDIR)/usr/bin/add-remove-locales

	rm -f $(DESTDIR)/usr/share/applications/tealocale.desktop

	rm -f $(DESTDIR)/usr/share/polkit-1/actions/com.tearch.tealocale.policy

	rm -f $(DESTDIR)/usr/share/locale/*/LC_MESSAGES/tealocale.mo