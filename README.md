# Tealocale

Locale installation & selection tool. Arch linux port of [mintlocale](https://github.com/linuxmint/mintlocale/)

## Dependencies

 - glib2
 - gtk3
 - gdk-pixbuf2
 - accountsservice
 - iso-flag-png
 - python
 - python-gobject
 - xapp
 - gettext

## Installation
You can install tealocale by `make DESTDIR=/`.
